$.validator.addMethod('uaphone', function(value, element, params){
	var regex = /\+38 \(0[0-9]{2}\) [0-9]{3}-[0-9]{2}-[0-9]{2}/g;
	var match = value.match(regex);
	if (match!==null) {
		if (match.length === 1) return true;
	}
	return false;
});
$(document).ready(function(){
	$('input[name="phone"]').mask('+38 (999) 999-99-99');
	var forms = document.getElementsByTagName('form');
	for (var i = 0; i < forms.length; i++) {
		$(forms[i]).validate({
			rules: {
				phone: {
					required: true,
					uaphone: true,
				},
			},
			messages: {
				phone: {
					required: 'Введите номер телефона',
					uaphone: 'Введите корректный номер телефона'
				}
			},
			submitHandler: submitForm
		});
	};

	function submitForm(form, e){
		var data = $(form).serialize();
		//var text = $(form).find('button').html();
		var page = $(form).find('[name="tagmanager"]').val();
		$.ajax({
				url: 'sendmessage.php',
				type: 'POST',
				data: data,
				beforeSend: function(){
					$(form).find('input, button').attr('disabled', '');
					$(form).find('button').html('Отправляем...');
				}
			})
			.done(function(response) {
				$(form).find('input, button').removeAttr('disabled');
				$(form).find('input').val('');
				$(form).find('button').html('Отправленно');
				$('#popup-holder').fadeOut(500);

				dataLayer.push({
					'event' : 'VirtualPageview',
					'virtualPageURL' : page,
					'virtualPageTitle' : page.replace('/', '')
				});
			})
			.fail(function(response) {
				console.log(response);
			});
	}

	$('#items').find('.button').on('click', function() {
		var toggle = $(this).siblings('.title').text();
		$('#popup').find("[name='order']").val(toggle);
		var tagmanager = $(this).attr('data-tagmanager');
		$('#popup').find("[name='tagmanager']").val(tagmanager);
		showPopup();
	});
	$('[data-info]').on('click', function() {
		var toggle = $(this).attr('data-info');
		$('#popup').find("[name='order']").val(toggle);
		var tagmanager = $(this).attr('data-tagmanager');
		$('#popup').find("[name='tagmanager']").val(tagmanager);
		showPopup();
	});
	$('#gallery').bxSlider({
		mode: 'fade',
		auto: true,
		controls: false,
		easing: 'ease-in'
	});
	//$('#items').bxSlider({
	//	auto: false,
	//	controls: true,
	//	slideWidth: 259,
	//	minSlides: 4,
	//	maxSlides:4,
	//	slideMargin: 20,
	//	moveSlides: 1,
	//	easing: 'linear'
	//});
});

//Функция показывает модальное окно + вычислияет позиционирования модального окна
// - по центру экрана независимо от его разрешения.

function showPopup(){

	$('#popup-holder').fadeIn(500);

	$('#popup-holder').append('<div class="closebox" style="position:absolute; width:100%; height:100%; left:0; top:0; z-index:50;">');
	var winWidth = window.innerWidth;
	var winHeight = window.innerHeight;
	var popPadding = parseInt($('#popup').css('padding'));
	var popWidth = $('#popup').width();
	var popHeight = $('#popup').height();
	var leftPosition = (winWidth/2) - ((popWidth/2) + (popPadding*2));
	var topPosition = (winHeight/2) - (popHeight/2);

	$('#popup').css('left', leftPosition);
	$('#popup').css('top', topPosition);

	$('#popup > .close').click(function(){
		$('#popup-holder').fadeOut(500);
	})
	if ($('div').is('.closebox')) {
		$('.closebox').click(function(){
			$('#popup-holder').fadeOut(500);
			$('.closebox').remove();
		})

	}
}
