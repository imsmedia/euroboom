<?php session_start();
$site_url = 'http://euroboom.com.ua/';
if (array_key_exists('utm_referrer', $_GET)) {
	if (strpos($site_url, $_GET['utm_referrer'])===false) {
		$_SESSION['referer'] = $_GET['utm_referrer'];
	}
}
if (array_key_exists('utm_source', $_GET)) {
	if (strpos($site_url, $_GET['utm_source'])===false) {
		$_SESSION['sourse'] = $_GET['utm_source'];
	}
	if (strpos($site_url, $_GET['utm_term'])===false) {
		$_SESSION['term'] = $_GET['utm_term'];
	}
	if (strpos($site_url, $_GET['utm_campaign'])===false) {
		$_SESSION['campaign'] = $_GET['utm_campaign'];
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" /> 
	<!--<meta name="viewport" content="width=device-width" />-->
	<meta name="viewport" content="width=1200" />

	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/resize.css" />
	<link rel="stylesheet" href="css/bxslider.css" />
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&subset=latin,cyrillic' rel='stylesheet' type='text/css' />

	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/maskedinput.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

	<title>Magnit</title>
</head>
<body>
<!-- Google Tag Manager -->
<script>dataLayer=[];</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5T6N8B"
				  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5T6N8B');</script>
<!-- End Google Tag Manager -->
	<div class="wrapper">
		<header>
			<div class="header-left">
				<div class="logo">
				<img src="img/logo.png" alt="logo" />
				<!-- Лого вырезал картинкой, но если это не так, закомментированная часть - лого средствами html/css -->
					<!-- <div class="css-logo">
						<p>лого</p>
					</div> -->
				</div>
				<!--<div class="header-description">-->
					<!--<p>Неодимовые магниты</p>-->
					<!--<p><span>Доставка по Украине</span></p>-->
				<!--</div>-->
			</div>
			<div class="header-center">
				Доставка по всей<br> Украине
			</div>
			<div class="header-right">
				<div class="phone">
					<a href="tel: +380686205010">+38(068)620-50-10</a>
					<a href="tel: +380664621842">+38(066)462-18-42 </a>
					<p>Работаем без выходных</p>
				</div>
				<button type="button" class="button1 header" data-tagmanager="/take-consult.html"  data-info="Заказать обратный звонок" id="753211">Заказать обратный звонок</button>
			</div>
		</header>

		<main>
			<section id="head">
				<img src="img/madein.png" class="head-sticker" alt="">
				<div class="head-title">
					<h1>Неодимовые магниты</h1>
					<h2>Решение проблемы высоких тарифов на коммунальные услуги</h2>
				</div>
				<div class="head-description">
					<p>Мощные магниты для всех счетчиков</p>
					<p><span>Гарантия качества и оперативности доставки</span></p>
				</div>
				<div class="head-deliv">
					<span>В случае необходимости возврат в течение 10 дней гарантированно</span>
				</div>
				<form class="form headform" method="POST">
					<div class="form-title headform">
						<p>Закажите магнит с доставкой по Украине</p>
					</div>
					<img src="img/magnit12.png" class="form-image" alt="">
					<div class="box">
						<div class="form-description howitworks">
							<p>от <b>234 грн.</b></p>
						</div>
						<div style="position: relative"><input class="input headform" type="text" placeholder="Введите телефон" name="phone"/></div>
						<input type="hidden" name="tagmanager" value="/magnit-with-deliv.html"/>
						<button class="button head-form" type="submit" id="8985">Заказать</button>
					</div>
					<div class="form-callback-text">
						<p>Перезвоним и поможем с подбором</p>
					</div>
				</form>
			</section>
			<section id="store">
				<div class="store-title">
					<h2>Каталог неодимовых магнитов</h2>
				</div>
				<div id="items">
					<div class="slide">
						<div class="title">	Магнит 30х10</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 30 мм<br>
							Высота: 10 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							90 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 45х25</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 45 мм<br>
							Высота: 25 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							300 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button" >Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 55х25</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 55 мм<br>
							Высота: 25 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							380 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 50х30</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 50 мм<br>
							Высота: 30 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							600 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 60х30</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 60 мм<br>
							Высота: 30 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							600 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 70х20</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 70 мм<br>
							Высота: 20 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							610 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 70х30</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 70 мм<br>
							Высота: 30 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							680 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 70х40</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 70 мм<br>
							Высота: 40 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							1050 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 70х50</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 70 мм<br>
							Высота: 50 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							1250 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 70х60</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 70 мм<br>
							Высота: 60 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							1500 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 90х40</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 90 мм<br>
							Высота: 40 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							2000 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 90х50</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 90 мм<br>
							Высота: 50 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							2300 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 100х40</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 100 мм<br>
							Высота: 40 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							2500 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
					<div class="slide">
						<div class="title">	Магнит 100х50</div>
						<img src="img/magnit2.jpg" alt="">
						<div class="fiches">
							Диаметр: 100 мм<br>
							Высота: 50 мм<br>
							Покрытие:Поверхность никелированная
						</div>
						<div class="price">
							2700 грн
						</div>
						<button data-tagmanager="/order-magnit.html" class="button">Заказать</button>
					</div>
				</div>
			</section>
			<section id="polland">
				<div class="polland-title">
					<h2>Прямые поставки с Польши</h2>
				</div>
			</section>
			<section id="howtosave">
				<div class="howtosave-title">
					<h2>Как экономить с неодимовым магнитом</h2>
				</div>
				<ul>
					<li>
						<div class="icon">
							<img src="img/icons/support.png" alt="support" />
						</div>
						<div class="howtosave-text">
							<p>Проконсультироваться <br />и заказать</p>
						</div>
					</li>
					<li>
						<div class="icon">
							<img src="img/icons/cash.png" alt="cash" />
						</div>
						<div class="howtosave-text">
							<p>Оплатить <br />при получении</p>
						</div>
					</li>
					<li>
						<div class="icon">
							<img src="img/icons/moneybox.png" alt="moneybox" />
						</div>
						<div class="howtosave-text">
							<p>Установить и <br />наслаждаться экономией</p>
						</div>
					</li>
				</ul>
			</section>

			<section id="specs">
				<div id="specs-container">
					<div class="title">
						<h6>260 385 <span>магнитов</span></h6>
						<p>куплено в Украине за последний год!</p>
					</div>
					<div class="gallery">
						<ul id="gallery">
							<li><img src="img/gallery/slide-1.jpg" alt="slide-1" /></li>
							<li><img src="img/gallery/slide-2.jpg" alt="slide-2" /></li>
							<li><img src="img/gallery/slide-3.jpg" alt="slide-3" /></li>
							<li><img src="img/gallery/slide-4.jpg" alt="slide-4" /></li>
						</ul>
					</div>
					<div class="specs">
						<ul>
							<li><p>Для любых счетчиков;</p></li>
							<li><p>Цена от 234 грн за магнит;</p></li>
							<li><p>Материал: Неодим + Железо + Бор;</p></li>
							<li><p>Защитный никелевый слой;</p></li>
							<li><p>Степень намагниченности: №42;</p></li>
							<li><p>Сила удержания: до 1000 кг;</p></li>
							<li><p>Срок службы: более 20 лет;</p></li>
							<li><p>Доставка в любую точку Украины за 2 дня!</p></li>
						</ul>
					</div>
					<div class="clearbox"></div>
					<div class="order">
						<div class="title">
							<p>Все давно пользуются - вы последний!</p>
						</div>
						<div style="display: none;" class="title">Заказать со скидкой!</div>
						<button class="button ordermagnit" data-info="Заказать со скидкой"  data-tagmanager="/order-with-sale.html" id="2322">Заказать со скидкой!</button>
					</div>
				</div>
			</section>

			<section id="howitworks">
				<div class="title">
					<h3>Как это работает</h3>
				</div>
				<div class="howitworks-container">
					<div class="howitworks-left">
						<img src="img/count-howitworks.jpg" alt="count" />
					</div>
					<div class="howitworks-right">
						<div class="howitworks-description">
							<p><strong>Принцип работы магнита прост</strong> - сильное магнитное поле блокирует работу металлических элементов систем учета потребления в счетчиках.</p>
							<p><span>В результате счетчик не считает, а вы не переплачиваете!</span></p>
							<p>Неодимовые магниты действуют практически на любые счетчики, но в зависимости от системы, требуются магниты разной мощности и формы.</p>
						</div>
						<form  class="form howitworks"  method="POST">
							<fieldset>
								<div class="form-title howitworks">
									<p>Подбери магнит<br /> для своего счетчика</p>
								</div>
							</fieldset>
							<fieldset>
								<input class="input howitworks" type="text" placeholder="Модель счетчика" name="model"/>
								<input class="input howitworks" type="text" placeholder="Ваш телефон" name="phone"/>
								<input type="hidden" name="tagmanager" value="/for-model.html"/>
								<button class="button howitworks" type="submit" id="666">Подобрать</button>
							</fieldset>
						</form>
					</div>
				</div>
			</section>

			<!--<section id="health">-->
				<!--<div class="clearbox"></div>-->
				<!--<div class="consult">-->
					<!--<div class="consult-left">-->
						<!--<div class="title">-->
							<!--<h6 style="line-height: 50px;font-size: 25px;">Получите цены для оптовых продаж</h6>-->
							<!--<p>Узнайте цену у специалиста</p>-->
						<!--</div>-->
					<!--</div>-->
					<!--<div class="consult-right">-->
						<!--<button type="button" class="button1 consultbtn" onclick="showPopup()">Узнать</button>-->
					<!--</div>-->
					<!--<div class="clearbox"></div>-->
				<!--</div>-->
			<!--</section>-->

			<div class="findout third">
				<div class="findout-container">
					<div class="findout-left">
						<div class="title-findout">
							<div class="title">
								<h6 style="font-size: 30px;font-weight: bold;margin-bottom: 10px;">Специальные условия и цены<br> для оптовых покупателей</h6>
								<p style="font-size: 20px;color:black">Узнайте как заработать с неодимовыми магнитами</p>
							</div>
						</div>
					</div>
					<div class="findout-right">
						<form class="form findout"  method="POST">
							<fieldset>
								<input class="input findout" type="text" placeholder="Введите телефон" name="phone"/>
								<input type="hidden" name="tagmanager" value="/opt.html"/>
								<button class="button1 findout" type="submit" id="122121" >Узнать подробности</button>
							</fieldset>
						</form>
					</div>
					<div class="clearbox"></div>
				</div>
			</div>

			<section id="comment">
				<div class="comment-container">
					<div class="title">
						<h3>Комментарий юриста</h3>
					</div>
					<div class="comment-left">
						<div class="photo">
							<img src="img/jurist.png" alt="photo" />
						</div>
						<div class="name">
							<p>Павлов Дмитрий Александрович, юрист</p>
						</div>
					</div>
					<div class="comment-right">
						<div class="comments">
							<p>За всю историю Украины ни один человек не был привлечен к ответственности за манипуляции со счетчиками при помощи неодимовых магнитов.</p>
							<p>Продажа и покупка магнитов абсолютно законна. В случае, если покупатель магнита будет использовать его в целях экономии на услугах ЖКХ, доказать факт противоправных действий абсолютно невозможно. Магнит не оставляет никаких следов на счетчике, а разница в потреблении может быть обусловлена вполне объективными причинами и не может быть доказательством в суде.</p>
							<p>Кроме того, отсутствие какого-либо контроля со стороны государства над монополистами привела к тому, что значительно выросшие тарифы для предприятий с избытком перекроют манипуляции любых частных потребителей - на разбирательство в этих вопросах никто не будет тратить время.</p>
						</div>
					</div>
					<div class="clearbox"></div>
				</div>
			</section>
			
			<div class="findout first">
				<div class="findout-container">
					<div class="findout-left">
						<div class="title-findout">
							<p>Узнай о законных способах экономии с неодимовым магнитом</p>
						</div>
					</div>
					<div class="findout-right">
						<form class="form findout"  method="POST">
							<fieldset>
								<input class="input findout" type="text" placeholder="Введите телефон" name="phone"/>
								<input type="hidden" name="tagmanager" value="/layer.html"/>
								<button class="button1 findout" type="submit" id="456498777">Узнать</button>
							</fieldset>
						</form>
					</div>
				</div>
				<div class="clearbox"></div>
			</div>
			

			<section id="whyus">
				<div class="title">
					<h4>Почему именно наши магниты</h4>
				</div>
				<div class="whyus-left">
					 <ul>
					 	<li>
					 		<div class="whyus-icon">
					 			<img src="img/icons/shield.png" alt="shield" />
					 		</div>
					 		<div class="whyus-text">
					 			<p>20 лет<br /> гарантия</p>
					 		</div>
					 		<div class="clearbox"></div>
					 	</li>
					 	<li>
					 		<div class="whyus-icon">
					 			<img src="img/icons/nocash.png" alt="nocash">
					 		</div>
					 		<div class="whyus-text">
					 			<p>Без<br /> предоплаты</p>
					 		</div>
					 		<div class="clearbox"></div>
					 	</li>
					 	<li>
					 		<div class="whyus-icon">
					 			<img src="img/icons/layer.png" alt="layer">
					 		</div>
					 		<div class="whyus-text">
					 			<p>Защитный никелевый слой</p>
					 		</div>
					 		<div class="clearbox"></div>
					 	</li>
					 </ul>
				</div>
				<div class="whyus-center">
					<img src="img/magnit2.jpg" alt="magnit" />
				</div>
				<div class="whyus-right">
					<ul>
						<li>
					 		<div class="whyus-icon">
					 			<img src="img/icons/truck.png" alt="truck">
					 		</div>
					 		<div class="whyus-text">
					 			<p>2 дня доставка<br /> по Украине</p>
					 		</div>
					 		<div class="clearbox"></div>
					 	</li>
					 	<li>
					 		<div class="whyus-icon">
					 			<img src="img/icons/battery.png" alt="battery">
					 		</div>
					 		<div class="whyus-text">
					 			<p>Самый мощный из существующих</p>
					 		</div>
					 		<div class="clearbox"></div>
					 	</li>
					 	<li>
					 		<div class="whyus-icon">
					 			<img src="img/icons/plant.png" alt="plant">
					 		</div>
					 		<div class="whyus-text">
					 			<p>Прямые поставки от производителя</p>
					 		</div>
					 		<div class="clearbox"></div>
					 	</li>
				 	</ul>
				</div>
				<div class="clearbox"></div>
			</section>

			<div class="findout second">
				<div class="findout-container">
					<div class="findout-left">
						<div class="title-findout">
							<p>Узнай стоимость магнита для своего счетчика</p>
						</div>
					</div>
					<div class="findout-right">
						<form  class="form findout"  method="POST">
							<fieldset>
								<input class="input findout" type="text" placeholder="Введите телефон" name="phone"/>
								<input type="hidden" name="tagmanager" value="/price-for.html"/>
								<button class="button1 findout" type="submit" id="778">Узнать</button>
							</fieldset>
						</form>
					</div>
					<div class="clearbox"></div>
				</div>
			</div>

			<section id="health">
				<div class="title">
					<h4>Влияет ли магнит на здоровье?</h4>
				</div>
				<div class="health-left">
					<div class="health-left-block left-element">
						<div class="health-element nd">
							<p>Nd</p>
						</div>
					</div>
					<div class="health-left-block">
						<div class="health-text fe">
							<p>Элементы, входящие в состав магнита, не токсичны и абсолютно безвредны. Воздействие магнитного поля на организм человека также безвредно. Более того, существуют свидетельства благотворного влияния мощных магнитов на здоровье человека. Основным производителем редкоземельных металлов является Китай (до 95% мирового производства). Не удивительно, что именно в традиционной Китайской медицине впервые стали использоваться неодимовые магниты.</p>
						</div>
					</div>
					<div class="health-left-block left-element">
						<div class="health-element b">
							<p>B</p>
						</div>
					</div>
				</div>
				<div class="health-right">
					<div class="health-right-block">
						<div class="health-text nd">
							<p><strong>Неодим (Nd)</strong> — 60 элемент периодической таблицы Менделеева, редкоземельный металл, относящийся к разряду лантаноидов. В соединении с железом и бором (химическая формула Nd14Fe21B) является самым сильным постоянным магнитом из известных науке в наше время.</p>
						</div>
					</div>
					<div class="health-right-block right-element">
						<div class="health-element fe">
							<p>Fe</p>
						</div>
					</div>
					<div class="health-right-block">
						<div class="health-text b">
						   <p>Улучшение зрения, циркуляции крови и обмена веществ, ускорение заживления ран, нормализация артериального давления крови и работы капилляров, сосудов, суставов, органов дыхательных путей, профилактика многих тяжелых заболеваний, все это можно сделать благодаря грамотному воздействию на человеческий организм неодимовым магнитом.</p>
						</div>
					</div>
				</div>
				<div class="clearbox"></div>
				<div class="consult">
					<div class="consult-left">
						<div class="title">
							<h6>Узнайте, как неодимовый магнит поможет именно Вам!</h6>
							<p>Проконсультируйтесь со специалистом</p>
						</div>
					</div>
					<div class="consult-right">
						<button type="button" data-info="Получить консультацию" class="button1 consultbtn"  data-tagmanager="/take-consult-popup.html" id="12316458">Получить консультацию</button>
					</div>
					<div class="clearbox"></div>
				</div>
			</section>

			<section id="measures">
				<div class="measures-container">
					<div class="title">
						<h4>Меры предосторожности</h4>
						<p>При работе с мощными магнитами</p>
					</div>
					<ul>
						<li>
							<div class="icon-measures">
								<img src="img/icons/medical.png" alt="medical" />
							</div>
							<div class="measures-text-title">
								<p>Берегите пальцы</p>
							</div>
							<div class="measures-text">
								<p>Неодимовые магниты чрезвычайно мощные - никогда не помещайте руку между магнитом и металлической поверхностью или другим магнитом!</p>
							</div>

						</li>
						<li>
							<div class="icon-measures">
								<img src="img/icons/temperature.png" alt="temperature" />
							</div>
							<div class="measures-text-title">
								<p>Не перегревайте</p>
							</div>
							<div class="measures-text">
								<p>При нагреве выше 80&ordm; магнит теряет свои свойства и может разрушиться</p>
							</div>

						</li>
						<li>
							<div class="icon-measures">
								<img src="img/icons/processor.png" alt="processor" />
							</div>
							<div class="measures-text-title">
								<p>Электроприборы</p>
							</div>
							<div class="measures-text">
								<p>Мощный магнит может повлиять или заблокировать работу электронных или механических устройств</p>
							</div>

						</li>
						<li>
							<div class="icon-measures">
								<img src="img/icons/delivery.png" alt="delivery" />
							</div>
							<div class="measures-text-title">
								<p>Транспортировка</p>
							</div>
							<div class="measures-text">
								<p>Перевозки магнитов допускается только в надежной упаковке, не доспускающей взаимодействия магнита в окружающими предметами</p>
							</div>

						</li>
					</ul>
				</div>
			</section>
		</main>

		<footer>
			<div class="title">
				<h5>Остались вопросы?</h5>
				<p>Оставьте заявку и специалисты<br /> нашего колл-центра ответят на все ваши вопросы</p>
			</div>
			<form class="form footer"  method="POST">
				<fieldset>
					<input type="text" class="input footer" placeholder="Введите телефон" name="phone"/>
					<input type="hidden" name="tagmanager" value="/have-some-question.html"/>
					<button type="submit" class="button footer" id="485487487">Подобрать</button>
				</fieldset>
			</form>
		</footer>

		<div id="popup-holder">
			<div id="popup">
				<div class="close" title="Закрыть">X</div>
				<div class="title">
					<p>Оставьте свои контактные данные<br>
						и менеджер перезвонит вам<br> через 10 минут.</p>
				</div>
				<form class="form popup"  method="POST">
					<fieldset>
						<input type="text" class="input popup" placeholder="Ваше имя" name="name"/>
						<input type="text" class="input popup" placeholder="Ваш телефон" name="phone"/>
						<input type="hidden" name="order" value="">
						<input type="hidden" name="tagmanager" value="">
						<button type="submit" class="button1 popup" id="1212222">Перезвонить мне</button>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</body>
</html>